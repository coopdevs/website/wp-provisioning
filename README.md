# Ansible scripts to provision and deploy Wordpress

These are [Ansible](http://docs.ansible.com/ansible/) playbooks (scripts) for managing an [Wordpress](https://wordpress.org/) instance.

## Requirements

You will need Ansible on your machine to run the playbooks.
These playbooks will install the MariaDB database, PHP, NGINX server and Wordpress stack.

It has currently been tested on hosts running

* Ubuntu 18.04 **Bionic** (amd64)

### Environment requirements

In order to assure that the playbooks contained here are run always with the same ansible version and linted the same way, we are using [`pyenv`](https://github.com/pyenv/pyenv).

Follow [Installing and using pyenv](https://github.com/coopdevs/handbook/wiki/Installing-and-using-pyenv), or, in short:

- [ ] Install pyenv (the script-based way is simple) `curl https://pyenv.run | bash`
- [ ] Install a local python version. Choose **`3.7`** `pyenv install 3.7`
- [ ] Create a virtual env with pyenv for this project `pyenv virtualenv 3.7 odoo-provisioning`
- [x] Activate the venv (this is automatic in pyenv when you cd into the dir with .python-version)
- [ ] Install required tools with local pip `(odoo-provisioning) $ pip install -r requirements.txt`

### Role requirements

Install dependencies running:
```
ansible-galaxy install -r requirements.yml
```

## Inventory

You need to adapt the inventory folder with the `hosts` file and the `group_vars` and `host_vars` file. See the [Ansible doc about inventories](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html).
For more dettails on how to create a compatible inventory, see the [wiki](https://gitlab.com/coopdevs/odoo-provisioning/wikis/How%20to%20create%20a%20new%20Odoo%20instance).

You can always use an external inventory with the `-i` parameter when running `ansible-playbook`.

## Playbooks

### sys_admins.yml

This playbook uses the community role [sys-admins-role](https://github.com/coopdevs/sys-admins-role).

This playbook will prepare the host to allow access to all the system administrators.

```yaml
# playbooks/my_playbook.yml
- name: Create all the users for system administration
  roles:
    - role: coopdevs.sys-admins-role
      vars:
        sys_admin_group: sysadmin
        sys_admins: "{{ system_administrators }}"
```

In each environment (`dev`, `staging`, `production`) we can find the list of users that will be created as system administrators.

We use `host_vars` to declare per environment variables:
```yaml
# <YOUR_INVENTORY_FOLDER>/inventory/host_vars/<YOUR_HOST>/config.yml

system_administrators:
  - name: pepe
    ssh_key: "../pub_keys/paula.pub"
    state: present
    - name: paco
    ssh_key: "../pub_keys/georgine.pub"
    state: present
```

Before execute the playbook, be sure that the needed keys are present in `pub_keys` folder.
The first time you run it against a brand new host you need to run it as `root` user.
You'll also need passwordless SSH access to the `root` user.
```
ansible-playbook playbooks/sys_admins.yml --limit=<environment_name> -u root -i <YOUR_INVENTORY_FILE>
```

For the following executions, the script will assume that your user is included in the system administrators list for the given host.

For example in the case of `development` environment the script will assume that the user that is running it is included in the system administrators [list](https://github.com/coopdevs/timeoverflow-provisioning/blob/master/inventory/host_vars/local.timeoverflow.org/config.yml#L5) for that environment.

To run the playbook as a system administrator just use the following command:
```
ansible-playbook playbooks/sys_admins.yml --limit=dev
```
Ansible will try to connect to the host using the system user. If your user as a system administrator is different than your local system user please run this playbook with the correct user using the `-u` flag.
```
ansible-playbook playbooks/sys_admins.yml --limit=dev -u <username>
```

### provision.yml

Installs and configures all required software on the server.

This is the main playbook. This playbook install all the dependencies and the WP version that you define in the inventory. Install and update also the modules defined in the inventory repository.

The playbook use different community roles to perform the complete WP installation. Also use our own roles to manage the monitoring and backups policies.

## Community roles:
* Security - [geerlingguy.security](https://github.com/geerlingguy/ansible-role-security)

This role changes the minimum security configuration. More information in the role description.


* PPA-Ondrej - [iambryancs.ppa-ondrej](https://github.com/iambryancs/ansible-role-ppa-ondrej)

Role to add the latests PHP versions to sources list.

* MySQL - [geerlingguy.mysql](https://github.com/geerlingguy/ansible-role-mysql)

Role to install MariaDB\MySQL in the different environments.

You need to declare the following variables:
```yaml
mysql_packages:
  - mariadb-client
  - mariadb-server
  - python-mysqldb

# Databases.
mysql_databases:
    - name: "{{ wp_mariadb_db  }}"

# Users.
mysql_users:
   - name:
     password:
```

* PHP - [geerlingguy.php](https://github.com/geerlingguy/ansible-role-php)
Role to install PHP and its extensions.

You need to declare at least the following variables:
```yaml
php_default_version_debian: 8.0
php_packages:
  - php{{ php_default_version_debian }}
  - php{{ php_default_version_debian }}-cli
  - php{{ php_default_version_debian }}-common
  - php{{ php_default_version_debian }}-gd
  - php{{ php_default_version_debian }}-mbstring
  - php{{ php_default_version_debian }}-pdo
  - php{{ php_default_version_debian }}-xml
  - php{{ php_default_version_debian }}-fpm
  - php{{ php_default_version_debian }}-apcu
  - php{{ php_default_version_debian }}-mysql
```


## Roles
### WordPress CLI - `wp-cli`

This role installs the command line client for WordPress, which allows to install and manage WP instances.

```yaml
wordpress_wp_cli_install_dir:                 # Path to folder where wp-cli will be installed
```

### Wordpress - `wordpress`

This role installs the Wordpress core and configure it. Optionally, its also installs plugins and themes.

```yaml
wp_mariadb_user:                            # Database user
wp_mariadb_password:                        # Database user's pass
wp_mariadb_db:                              # Database name

wp_title:                                   # Wordpress title
wp_admin_user:                              # WP admin user
wp_admin_mail:                              # WP admin mail
wp_admin_password:                          # WP admin pass
wp_directory:                               # WP installation path

wp_sys_user:                                # WP system user
wp_plugins:                                 # One or more (space-separated) plugins to install. [slug, local or URL to zip file]
wp_theme_slug:                              # Theme slug to install. Required to install a theme.
wp_theme_url:                               # Theme zip to install. [local or URL to zip file]. Optional.
wp_child_theme_slug:                        # Parent theme slug to install. Required if there is a child theme.
wp_child_theme_url:                         # Parent theme zip to install. [local or URL to zip file]. Optional.
```
### Backup role - `backup`
The backup role implements the community `coopdevs.backups_role` and adapt it to Wordpress. It dumps the WP database and copy the content of <TBA> and create an encrypted tarball with both.

The `coopdevs.backups_role` uploads it to a Restic repo, produces a parseable log and manages the rotation of old backups.

```yaml
wp_backups_pass:                            # The password to encrypt the tarball
```
# Development

In the development environment (`wordpress.local`) you can use the sysadmin user `wp`.

`ssh wp@wordpress.local`

### Install pre-commit hooks

We use [pre-commit framework](https://pre-commit.com/) to assure quality code.

```sh
pre-commit install
```

## Using LXC Containers

In order to create a development environment, we use the [devenv](https://github.com/coopdevs/devenv) tool. It uses [LXC](https://linuxcontainers.org/) to isolate the development environment.

This tool searches a configuration dotfile in the working directory. Please create one in your inventory if you want to take advantatge of this tool. An example [.devenv](/.devenv) file is present in the repository.


0. Spin the container:
```bash
devenv
```
## Provisioning

1. Run the `sys_admins` playbook:
```bash
ansible-playbook playbooks/sys_admins.yml -i inventory/hosts --limit=env -u root
```
2. Run the `provision` playbook:
```bash
ansible-playbook playbooks/provision.yml -i inventory/hosts --limit=env
```

3. Run the `deployment` playbook:
```bash
ansible-playbook playbooks/deployment.yml -i inventory/hosts --limit=env
```